import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { RestConfig } from '../model/static/RestConfig';
import { Articulo } from '../model/Articulo';
import { Clasificacion } from '../model/Clasificacion';

@Injectable()
export class KardexService {

    private kardexURL: string;

    private _configuration: RestConfig = new RestConfig();

    constructor(private http: HttpClient) {
        this.kardexURL = this._configuration.ServerWithApiUrl;
    }

    public getArticuloPorId<Articulo>(id: number): Observable<Articulo> {
        let url = `${this._configuration.ServerWithApiUrl}obtnerArticuloPorId/${id}`;
        return this.http.get<Articulo>(this.kardexURL);
    }

    public registrarArticulo(articulo: Articulo) {
        let urlRegistro = `${this.kardexURL}registrarArticulo`;
        return this.http.post(urlRegistro, articulo);
    }

    public obtenerClasificacioes<Clasificacion>(): Observable<Clasificacion> {
        let url = `${this._configuration.ServerWithApiUrl}obtenerClasificacioes`;
        return this.http.get<Clasificacion>(url);
    }

    public obtenerCatalogos(): Observable<any> {
        let url = `${this._configuration.ServerWithApiUrl}obtenerCatalogos`;
        return this.http.get(url);
    }

    public obtnerArticulos(): Observable<any> {
        let url = `${this._configuration.ServerWithApiUrl}obtnerArticulos`;
        return this.http.get(url);
    }


}