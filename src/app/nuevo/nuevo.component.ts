import { Component, OnInit } from '@angular/core';

import { Articulo } from '../model/Articulo';
import { KardexService } from '../services/KardexService';
import { Clasificacion } from '../model/Clasificacion';

@Component({
  selector: 'app-nuevo',
  templateUrl: './nuevo.component.html',
  styleUrls: ['./nuevo.component.css']
})
export class NuevoComponent implements OnInit {

  private _kardexService: KardexService;

  public articulo: Articulo = new Articulo();

  public clasificaciones: any;

  public clasificacionSeleccionada: number;

  public catalogos: any;

  public catalogoSeleccionado: number;

  public mensajeAlerta: string;

  public mostrarAlerta: boolean;

  constructor(_kardexService: KardexService) {
    this._kardexService = _kardexService;
  }

  ngOnInit() {
    this.clasificacionSeleccionada = null;
    this.catalogoSeleccionado = null;
    this.mensajeAlerta = null;
    this.obtenerClasificacioes();
    this.obtenerCatalogos();
  }

  obtenerClasificacioes() {
    this._kardexService.obtenerClasificacioes().
      subscribe(data => {
        this.clasificaciones = data;
      });
  }

  obtenerCatalogos() {
    this._kardexService.obtenerCatalogos().
      subscribe(data => {
        this.catalogos = data;
      });
  }

  crearArticulo() {
    console.log(this.articulo);
    this._kardexService.registrarArticulo(this.articulo).
      subscribe(data => {
        this.mensajeAlerta = 'Nuevo artículo creado exitosamente.';
        this.articulo = new Articulo();
        this.clasificacionSeleccionada = null;
        this.catalogoSeleccionado = null;
      });
  }

  seleccionarClasificacion($event) {
    this.articulo.idClasificacion = this.clasificacionSeleccionada;
  }

  seleccionarCatalogo($event) {
    this.articulo.idCatalogo = this.catalogoSeleccionado;
  }

  closeAlert() {
    this.mensajeAlerta = null;
  }

}
