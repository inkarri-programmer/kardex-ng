import { Component, OnInit } from '@angular/core';

import { KardexService } from '../services/KardexService';
import { Articulo } from '../model/IArticulo';

@Component({
  selector: 'app-venta',
  templateUrl: './venta.component.html',
  styleUrls: ['./venta.component.css']
})
export class VentaComponent implements OnInit {

  private _kardexService: KardexService;

  public articulo: Articulo = null;

  columnDefs = [
    { headerName: 'Articulo', field: 'nombreproducto' },
    { headerName: 'Precio', field: 'precio' },
    { headerName: 'Existencia', field: 'existencia' },
    { headerName: 'Tipo', field: 'clasificacionproducto.nombreclasificacion' },
    { headerName: 'Origen', field: 'catalogo.nombrecatalogo' }
  ];

  rowData: any[];

  constructor(_kardexService: KardexService) {
    this._kardexService = _kardexService;
  }

  ngOnInit() {
    this._kardexService.obtnerArticulos().
    subscribe(data => {
      this.rowData = data;
    });
  }

}
