import { Catalogo } from "./Catalogo";
import { Clasificacion } from "./Clasificacion";

export class Articulo {

    id: number;

    idCatalogo: number;

    idClasificacion: number;;

    nombreproducto: string;

    precio: number;

    existencia: number;

    catalogo: Catalogo;

    clasificacionproducto: Clasificacion;

    constructor() { }



}