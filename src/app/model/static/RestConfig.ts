

export class RestConfig {

    public Server = 'http://localhost:8080';

    public ApiUrl = 'aurora-ws/rx/kardex';

    public ServerWithApiUrl = `${this.Server}/${this.ApiUrl}/`;

}
