export interface Articulo {

    id: number;
  
    nombreproducto: string;
  
    precio: number;

    existencia: number;

}
